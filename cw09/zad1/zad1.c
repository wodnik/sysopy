#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define log(msg) \
        do { printf("LOG %d\n", msg); } while (0)
int threads;
char* filename;
int bufsize;
char* str_to_find;

int fd;

pthread_t *threads_tab;
pthread_key_t key;
pthread_mutex_t mutex;

void * task(void * a){
	sleep(1);
	int ret;
	void *buf = NULL;
	printf("Thread: %u\n", (unsigned) pthread_self());
	
	#ifndef VER_1
	ret = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);
	if (ret != 0) handle_error_en(ret, "pthread_setcancelstate");
	#else
	ret = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_setcancelstate");
	#endif
	
	#ifdef VER_1
	ret = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	if(ret != 0) handle_error_en(ret, "pthread_setcanceltype");
	#elif defined(VER_2)
	ret = pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
	if(ret != 0) handle_error_en(ret, "pthread_setcanceltype");
	#endif
	
	if ((buf = pthread_getspecific(key)) == NULL) {
		buf = malloc(bufsize);
		if (!buf) handle_error ("NULL POINTER EXCEPTION");
		ret = pthread_setspecific(key, buf);
		if (ret != 0) handle_error_en(ret, "pthread_setspecific");
	}
	
	while (1){
		ret = pthread_mutex_lock(&mutex);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock1");
		int readed = read(fd, buf, bufsize);
		int pos = lseek(fd, 0, SEEK_CUR);
		
		if (readed < 0) handle_error("read");
		if (readed == 0){
			ret = pthread_mutex_unlock(&mutex);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock_eof");
			break;
		}
		//printf("Thread: %u, positionXX: %d\n", (unsigned) pthread_self(), pos);
		
		char *ptr = strstr(buf, str_to_find);
		#ifdef VER_2
		ret = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		if (ret != 0) handle_error_en(ret, "pthread_setcancelstate");
		ret = pthread_mutex_unlock(&mutex);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock_v2");
		pthread_testcancel();
		ret = pthread_mutex_lock(&mutex);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock_v2");
		ret = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);
		if (ret != 0) handle_error_en(ret, "pthread_setcancelstate");
		#endif
		ret = pthread_mutex_unlock(&mutex);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock1");
		if (ptr){
			printf("Thread: %u, position: %ld\n", (unsigned) pthread_self(), (pos - readed) + ((void*)ptr - buf));
			//sleep(1);
			#ifndef VER_3
			for (int i = 0; i < threads; ++i){
				if (!pthread_equal(pthread_self(), threads_tab[i])){
					int ret = pthread_cancel(threads_tab[i]);
					if (ret != 0) handle_error_en(ret, "pthtread_cancel");
				}
			}
			ret = pthread_mutex_unlock(&mutex);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
			pthread_exit(0);
			#endif
		}
	}
	free(buf);
	#ifdef VER_3
	ret = pthread_detach(pthread_self());
	if (ret != 0) handle_error_en(ret, "pthread_detach");
	#endif
	
	pthread_exit(NULL);
}

int main(int argc, char **argv){
	
	if (argc != 5) {
		printf ("Usage: %s number_of_threads filename size_of_buffer string_to_find", argv[0]);
		exit(0);
	} else {
		threads = atoi(argv[1]);
		filename = argv[2];
		bufsize = atoi(argv[3]);
		str_to_find = argv[4];
	}
	
	threads_tab = malloc(sizeof(pthread_t) * threads);
	
	fd = open(filename, O_RDONLY);
	if (fd < 0) handle_error("open");

	pthread_attr_t attr;
	int ret = pthread_attr_init(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_init");
	
	ret = pthread_key_create(&key, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_key_create");
	
	ret = pthread_mutex_init(&mutex, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_init");
	
	for (int i = 0; i < threads; ++i){
		int ret = pthread_create(&threads_tab[i], &attr, task, NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_create");
	}
	#ifdef VER_3
	getchar();
	#endif
	#ifndef VER_3
	for (int i = 0; i < threads; ++i){
		int ret = pthread_join(threads_tab[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_join");
	}
	#endif
	ret = pthread_attr_destroy(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_destroy");
	
	ret = pthread_mutex_destroy(&mutex);
	if ( ret != 0) handle_error_en(ret, "pthread_mutex_destroy");
	close(fd);
	free(threads_tab);
	
	exit(0);
}
