#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define handle_error_noret(en, msg) \
        do { errno = en; perror(msg); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define log(msg) \
        do { printf("LOG %d\n", msg); } while (0)

int signals[] = {SIGUSR1, SIGTERM};
int threads;
pthread_t *threads_tab;
struct sigaction act, prev;
sigset_t set, oset;

void handler (int signo, siginfo_t* info, void*e)
{
	printf ("SIGNAL %d, PID: %ld, TID: %u\n", info->si_signo, (long)info->si_pid, (unsigned) pthread_self());
}

void * task(void * a){
	printf("Thread: %u: I'm in!\n", (unsigned) pthread_self());
	#ifdef VER_2
	sigemptyset(&set);
	pthread_sigmask(SIG_BLOCK, &set, &oset);
	#endif
	#ifdef VER_3
	act.sa_flags = SA_SIGINFO;
	act.sa_sigaction = handler;
	sigaction (SIGUSR1, &act, &prev);
	sigaction (SIGTERM, &act, &prev);
	#endif
	#ifdef VER_5
	act.sa_flags = SA_SIGINFO;
	act.sa_sigaction = handler;
	sigaction (SIGUSR1, &act, &prev);
	sigaction (SIGTERM, &act, &prev);
	#endif
	
	#ifdef VER_4
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigaddset(&set, SIGTERM);
	pthread_sigmask(SIG_BLOCK, &set, &oset);
	#endif

	while(1){
		printf("Już rano %u\n", (unsigned)pthread_self());
		sleep(4);
	}
	
	pthread_exit(0);

}

int main(int argc, char **argv){
	int signal;
	if (argc != 3) {
		printf ("Usage: %s number_of_threads signal\nsignal:\n\t 1 - SIGUSR1,\n\t2 - SIGTERM\n\t3 - SIGKILL\n\t4 - SIGSTOP\n", argv[0]);
		exit(0);
	} else {
		threads = atoi(argv[1]);
		signal = atoi(argv[2]);
		if (signal == 1) signal = SIGUSR1;
		else if (signal == 2) signal = SIGTERM;
		else if (signal == 3) signal = SIGKILL;
		else if (signal == 4) signal = SIGSTOP;
	}
	
	#ifdef VER_2
	errno = 0;
	sigset_t set, oset;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigaddset(&set, SIGTERM);
	pthread_sigmask(SIG_BLOCK, &set, &oset);
	perror("sig");
	#endif
	
	#ifdef VER_3
	act.sa_flags = SA_SIGINFO;
	act.sa_sigaction = handler;
	sigaction (SIGUSR1, &act, &prev);
	sigaction (SIGTERM, &act, &prev);
	#endif
	
	threads_tab = malloc(sizeof(pthread_t) * threads);

	pthread_attr_t attr;
	int ret = pthread_attr_init(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_init");
	
	#ifndef VER_5
	#ifndef VER_4
	for (int i = 0; i < threads; ++i){
		int ret = pthread_create(&threads_tab[i], &attr, task, NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_create");
	}
	#endif
	#endif
	
	#ifdef VER_5
		for (int i = 0; i < threads; ++i){
			int ret = pthread_create(&threads_tab[i], &attr, task, NULL);
			if (ret != 0) handle_error_en(ret, "pthtread_create");
		}
		sleep(1);
		for (int i = 0; i < threads-1; ++i){
			int ret = pthread_kill(threads_tab[i],signal);
			if (ret != 0) handle_error_en(ret, "pthtread_kill");
		}
		for (int i = 0; i < threads; ++i){
			int ret = pthread_join(threads_tab[i], NULL);
			if (ret != 0) handle_error_noret(ret, "pthtread_join");
		printf("\n\n");
	}
	#endif
	
	#ifdef VER_4
		for (int i = 0; i < threads; ++i){
			int ret = pthread_create(&threads_tab[i], &attr, task, NULL);
			if (ret != 0) handle_error_en(ret, "pthtread_create");
		}
		sleep(1);
		printf("Wysyłam sygnały\n");
		for (int i = 0; i < threads; ++i){
			int ret = pthread_kill(threads_tab[i],signal);
			if (ret != 0) handle_error_en(ret, "pthtread_kill");
		}
		for (int i = 0; i < threads; ++i){
			int ret = pthread_join(threads_tab[i], NULL);
			if (ret != 0) handle_error_noret(ret, "pthtread_join");
		}
		printf("\n\n");
	#endif
	
	for (int i = 0; i < threads; ++i){
		int ret = pthread_join(threads_tab[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_join");
	}
	
	ret = pthread_attr_destroy(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_destroy");

	free(threads_tab);
	
	exit(0);
}
