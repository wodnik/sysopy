#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define handle_error_noret(en, msg) \
        do { errno = en; perror(msg); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define log(msg) \
        do { printf("LOG %d\n", msg); } while (0)

int threads;
pthread_t *threads_tab;
struct sigaction act, prev;
sigset_t set, oset;

void handler (int a)
{
	//signal(SIGFPE, handler);
	printf ("ASDFGHJ\n");
	return;
}

void * task(void * a){
	printf("Thread: %u: I'm in!\n", (unsigned) pthread_self());
	
	//signal(SIGFPE, handler);
	
	sigemptyset(&set);
	sigaddset(&set, SIGFPE);	
	if (pthread_sigmask(SIG_SETMASK, &set, &oset) != 0) handle_error("pthread_sigmask");
	
	if ((long) a == 1){
		int b = 0;
		sleep(4);
		b = 5 / b;
	} else {
		while (1){
			printf("inny\n");
			sleep(1);
		}
	}
	
	pthread_exit(0);

}

int main(int argc, char **argv){

	if (argc != 2) {
		printf ("Usage: %s number_of_threads\n", argv[0]);
		exit(0);
	} else {
		threads = atoi(argv[1]);
	}
	//blokada SIGFPE, zobaczymy jak się zachowa wątek

	sigemptyset(&set);
	sigaddset(&set, SIGFPE);
	pthread_sigmask(SIG_SETMASK, &set, &oset);
	//pthread_kill(pthread_self(), SIGFPE);
	
	//signal(SIGFPE, handler);
	
	threads_tab = malloc(sizeof(pthread_t) * threads);

	pthread_attr_t attr;
	int ret = pthread_attr_init(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_init");

	for (long i = 0; i < threads; ++i){
		int ret = pthread_create(&threads_tab[i], &attr, task, (void*) i);
		if (ret != 0) handle_error_en(ret, "pthtread_create");
	}
	
	for (int i = 0; i < threads; ++i){
		int ret = pthread_join(threads_tab[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_join");
	}
	
	ret = pthread_attr_destroy(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_destroy");

	free(threads_tab);
	
	exit(0);
}
