#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define handle_error_noret(en, msg) \
        do { errno = en; perror(msg); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define log(msg) \
        do { printf("LOG %d\n", msg); } while (0)

int threads = 5;
pthread_t *threads_tab;
pthread_mutex_t forks[5];
pthread_mutex_t waiter[2];

pthread_key_t key;

int waiternum = 4;


void * philosopher(void * a){
	printf("Thread: %u: I'm in!\n", (unsigned) pthread_self());
	int ret;
	
	int *control = NULL;
	if ((control = pthread_getspecific(key)) == NULL) {
		control = malloc(sizeof(int));
		*control = 1;
		if (!control) handle_error ("NULL POINTER EXCEPTION");
		ret = pthread_setspecific(key, control);
		if (ret != 0) handle_error_en(ret, "pthread_setspecific");
	}
	
	while(1){
		//opuść semafor lokaja
		ret = pthread_mutex_lock(&waiter[0]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
		waiternum--;
		if(waiternum <= 0) {
			ret = pthread_mutex_unlock(&waiter[0]);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
			ret = pthread_mutex_lock(&waiter[1]);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
		} 
		pthread_mutex_unlock(&waiter[0]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");

		
		//podnieś widelce
		ret = pthread_mutex_lock(&forks[(long) a]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
		ret = pthread_mutex_lock(&forks[((long) a + 1) % 5]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
		
		//smacznego:)
		printf("Philosopher %ld: I'm eating!\n", (long) a);
		usleep(rand() % 5000);
		printf("Philosopher %ld: I'm not hungry!\n", (long) a);
		
		//opuść widelce
		ret = pthread_mutex_unlock(&forks[(long) a]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
		ret = pthread_mutex_unlock(&forks[((long) a + 1) % 5]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
	
		//podnieś semafor lokaja
		ret = pthread_mutex_lock(&waiter[0]);
		if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
		waiternum++;
		if(waiternum<=1) {
			ret = pthread_mutex_unlock(&waiter[1]);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
		} else {
			ret = pthread_mutex_unlock(&waiter[0]);
			if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
		}
	}
}

int main(int argc, char **argv){
	int ret;
	threads_tab = malloc(sizeof(pthread_t) * threads);
	
	ret = pthread_key_create(&key, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_key_create");
	
	for (int i = 0; i < 5; ++i){
		ret = pthread_mutex_init(&forks[i], NULL);
		if ( ret != 0) handle_error_en(ret, "pthread_mutex_init");
	}
	for (int i = 0; i < 2; ++i){
		ret = pthread_mutex_init(&waiter[i], NULL);
		if ( ret != 0) handle_error_en(ret, "pthread_mutex_init");
	}
	
	ret = pthread_mutex_lock(&waiter[1]);
	if ( ret != 0) handle_error_en(ret, "pthread_mutex_lock");
	
	pthread_attr_t attr;
	ret = pthread_attr_init(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_init");
	
	for (long i = 0; i < threads; ++i){
		int ret = pthread_create(&threads_tab[i], &attr, philosopher, (void*) i);
		if (ret != 0) handle_error_en(ret, "pthtread_create");
	}
	
	for (int i = 0; i < threads; ++i){
		int ret = pthread_join(threads_tab[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_join");
	}
	
	ret = pthread_attr_destroy(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_destroy");
	
	for (int i = 0; i < 5; ++i){
		ret = pthread_mutex_destroy(&forks[i]);
		if ( ret != 0) handle_error_en(ret, "pthread_mutex_destroy");
	}
	for (int i = 0; i < 2; ++i){
		ret = pthread_mutex_destroy(&waiter[i]);
		if ( ret != 0) handle_error_en(ret, "pthread_mutex_destroy");
	}
	
	free(threads_tab);
	
	exit(0);
}
