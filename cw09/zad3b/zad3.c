#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

#define handle_error_noret(en, msg) \
        do { errno = en; perror(msg); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)
        
#define log(msg) \
        do { printf("LOG %d\n", msg); } while (0)

int threads = 5;
pthread_t *threads_tab;

pthread_cond_t forks[5];
int taken[5];
pthread_mutex_t mforks;

pthread_cond_t waiter;

pthread_mutex_t printer;


int ph_at_the_table = 0;

void take(long i){
	int ret;
	
	ret = pthread_mutex_lock(&mforks);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
	
	if (ph_at_the_table == 4){
		ret = pthread_cond_wait(&waiter, &mforks);
		if (ret != 0) handle_error_en(ret, "pthread_cond_wait");
	}
	++ ph_at_the_table;
	
	if (taken[i]){
		ret = pthread_cond_wait(&forks[i], &mforks);
		if (ret != 0) handle_error_en(ret, "pthread_cond_wait");
	}
	taken[i] = 1;
	if (taken[(i+1) % 5]){
		ret = pthread_cond_wait(&forks[(i+1) % 5], &mforks);
		if (ret != 0) handle_error_en(ret, "pthread_cond_wait");
	}
	taken[(i+1) % 5] = 1;
	
	ret = pthread_mutex_unlock(&mforks);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
}
void put(long i){
	int ret;
	
	ret = pthread_mutex_lock(&mforks);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_lock");
	
	taken[i] = 0;
	ret = pthread_cond_signal(&forks[i]);
	if (ret != 0) handle_error_en(ret, "pthread_cond_signal");
	taken[(i+1) % 5] = 0;
	ret = pthread_cond_signal(&forks[(i+1) % 5]);
	if (ret != 0) handle_error_en(ret, "pthread_cond_signal");
	
	--ph_at_the_table;
	ret = pthread_cond_signal(&waiter);
	if (ret != 0) handle_error_en(ret, "pthread_cond_signal");
	
	ret = pthread_mutex_unlock(&mforks);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_unlock");
}

void * philosopher(void * a){
	printf("Thread: %u: I'm in!\n", (unsigned) pthread_self());

	while(1) {
		take((long) a);
		printf("Philosopher %ld: I'm eating!\n", (long) a);
		usleep(rand() % 5000);
		printf("Philosopher %ld: I'm not hungry!\n", (long) a);
		put((long) a);
	}
	
	pthread_exit(0);
}

int main(int argc, char **argv){
	int ret;
	threads_tab = malloc(sizeof(pthread_t) * threads);

	ret = pthread_mutex_init(&mforks, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_init");
	
	ret = pthread_mutex_init(&printer, NULL);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_init");
	
	for (long i = 0; i < 5; ++i){
		int ret = pthread_cond_init(&forks[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_cond_init");
	}
	ret = pthread_cond_init(&waiter, NULL);
	if (ret != 0) handle_error_en(ret, "pthtread_cond_init");
	
	pthread_attr_t attr;
	ret = pthread_attr_init(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_init");

	for (long i = 0; i < threads; ++i){
		int ret = pthread_create(&threads_tab[i], &attr, philosopher, (void*) i);
		if (ret != 0) handle_error_en(ret, "pthtread_create");
	}

	for (int i = 0; i < threads; ++i){
		int ret = pthread_join(threads_tab[i], NULL);
		if (ret != 0) handle_error_en(ret, "pthtread_join");
	}
	
	ret = pthread_attr_destroy(&attr);
	if ( ret != 0) handle_error_en(ret, "pthread_attr_destroy");

	ret = pthread_mutex_destroy(&mforks);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_destroy");
	
	ret = pthread_mutex_destroy(&printer);
	if (ret != 0) handle_error_en(ret, "pthread_mutex_destroy");

	ret = pthread_cond_destroy(&waiter);
	if (ret != 0) handle_error_en(ret, "pthtread_cond_destroy");

	free(threads_tab);
	
	exit(0);
}
